## Downloading the dataset
The dataset is available for download under the Dataset folder
(The original source: https://lhncbc.nlm.nih.gov/LHC-publications/pubs/MalariaDatasets.html)

## Creating an anaconda environment

```conda create -n FLworkshop python=3.6```

## Installing dependencies
- jupyter

```conda install -c anaconda jupyter```
- tensorflow

```conda install -c anaconda tensorflow-gpu```

- opencv

```conda install -c conda-forge opencv```

- PIL

```conda install -c conda-forge pillow```
- matplotlib

```conda install -c conda-forge matplotlib```
- scikit-learn

```conda install -c conda-forge scikit-learn```

- flower

```pip install flwr```
## Running FL scripts
- server: 
	```
    python server.py
    ```
- clients: 
	```
    python client.py --dataset_path ..\Dataset\Train\dataset_part1
	python client.py --dataset_path ..\Dataset\Train\dataset_part2
    ```

